import Slider from "react-slick";
import Link from 'next/link'

import titleStyles from "../styles/Titles.module.scss";

export default function Titles(cat, props) {

  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", background: "red" }}
        onClick={onClick}
      />
    );
  }
  
  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", background: "green" }}
        onClick={onClick}
      />
    );
  }
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    centerMode:true,
    centerPadding: "20px",
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  return (
    <>
      <div className={titleStyles.category}>{cat.name}</div>
      <div className={titleStyles.box}>
      <Slider {...settings}>
          <Link href="/video">
        <div>
          <img className={titleStyles.image} src="/matrix.jpg" />
        </div>
          </Link>
          <Link href="/video">
        <div>
          <img className={titleStyles.image} src="/matrix.jpg" />
        </div>
          </Link>
          <Link href="/video">
        <div>
          <img className={titleStyles.image} src="/matrix.jpg" />
        </div>
          </Link>
          <Link href="/video">
        <div>
          <img className={titleStyles.image} src="/matrix.jpg" />
        </div>
          </Link>
          <Link href="/video">
        <div>
          <img className={titleStyles.image} src="/matrix.jpg" />
        </div>
          </Link>
          <Link href="/video">
        <div>
          <img className={titleStyles.image} src="/matrix.jpg" />
        </div>
          </Link>
          <Link href="/video">
        <div>
          <img className={titleStyles.image} src="/matrix.jpg" />
        </div>
          </Link>
          <Link href="/video">
        <div>
          <img className={titleStyles.image} src="/matrix.jpg" />
        </div>
          </Link>
          <Link href="/video">
        <div>
          <img className={titleStyles.image} src="/matrix.jpg" />
        </div>
          </Link>
          <Link href="/video">
        <div>
          <img className={titleStyles.image} src="/matrix.jpg" />
        </div>
          </Link>
      </Slider>
      </div>
    </>
  );
}
