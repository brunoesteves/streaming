import RecommendationStyles from '../styles/Recommendation.module.scss'
import Image from "next/image";
import { BsFillPlayFill } from "react-icons/bs";

export default function Recommendation(){
  return(
    <div className={RecommendationStyles.container}>
      <Image src="/matrix.jpg" layout="responsive" width={160} height={80} />
     <div className={RecommendationStyles.info}>
        <div className={RecommendationStyles.company}>
          <Image src="/logo.png" width={160} height={30} />
        </div>
        <div className={RecommendationStyles.name}>Bushland</div>
        <div className={RecommendationStyles.rating_seasons}>
          <div className={RecommendationStyles.rating_}>18+</div>
          <div className={RecommendationStyles.seasons}>2 seasons</div>
        </div>
        <div className={RecommendationStyles.sinpose}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown
        </div>
        <div className={RecommendationStyles.play_more}>
          <div className={RecommendationStyles.play}>
            <BsFillPlayFill size="1.5em" /> Play Now
          </div>
          <div className={RecommendationStyles.more}>More Details</div>
        </div>
      </div>
    </div>
  )
}