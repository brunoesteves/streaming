import SliderStyles from "../styles/Slider.module.scss";
import Link from 'next/link'
import { BsFillPlayFill } from "react-icons/bs";
import Image from "next/image";

export default function Slider() {
  return (
    <div className={SliderStyles.new_content}>
      <Image src="/matrix.jpg" layout="fill" />
      <div className={SliderStyles.info_new_content}>
        <div className={SliderStyles.company_new_content}>
          <Image src="/logo.png" width={160} height={30} />
        </div>
        <div className={SliderStyles.name_new_content}>Bushland</div>
        <div className={SliderStyles.rating_seasons_new_content}>
          <div className={SliderStyles.rating_new_content}>18+</div>
          <div className={SliderStyles.seasons_new_content}>2 seasons</div>
        </div>
        <div className={SliderStyles.sinpose_new_content}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown
        </div>
        <div className={SliderStyles.play_more_new_content}>
          <Link href="/video">
          <div className={SliderStyles.play_new_content}>
            <BsFillPlayFill size="1.5em" /> Play Now
          </div>
            </Link>
          <div className={SliderStyles.more_new_content}>More Details</div>
        </div>
      </div>
    </div>
  );
}
