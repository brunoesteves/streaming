import Image from "next/image";
import Link from "next/link";
import MenuStyles from "../styles/Menu.module.scss";
import { BsSearch, BsPersonFill } from "react-icons/bs";
import { VscAccount } from "react-icons/vsc";

const Menu = () => {
  return (
    <div className={MenuStyles.content}>
      <Image src="/logo.png" alt="logotype" width={160} height={40} />
      <div>
        <Link href="/">
          <a className={MenuStyles.item}>Home</a>
        </Link>
        <Link href="/">
          <a className={MenuStyles.item}>Tv Show</a>
        </Link>
        <Link href="/">
          <a className={MenuStyles.item}>Movies</a>
        </Link>
      </div>
      <div className={MenuStyles.icon}>
        <div>
          <BsSearch size="1.5em" />
        </div>
        <div>
          <VscAccount size="1.5em" />
        </div>
        <div className={MenuStyles.icon_client}>
          <BsPersonFill size="1.5em" />
        </div>
      </div>
    </div>
  );
};

export default Menu;
