import Menu from "./Menu.js";
import Head from "next/head";
import LayoutStyles from "../styles/Layout.module.scss";


export default function Layout({ children }) {
  return (
    <>
      <Head>
        <title>Streamit</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={LayoutStyles.container}>
        <Menu />
        {children}
      </div>
    </>
  );
}
