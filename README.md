<h1>Projeto Streaming Filmes</h1>

Projeto em desenvolvimento de streaming para visualização de vídeos. Tecnologia utilizada é o ReactJS.

<h1>Instalação</h1>

Clone repository:

    https://gitlab.com/brunoesteves/streaming.git

Instalar dependências:

    npm install

<h1>Run application</h1>
    npm start

<h1>Contato</h1>

    Nome: Bruno Gomes Esteves
    Email: n3586@hotmail.com
    Linkedin: https://www.linkedin.com/in/brunogesteves/







