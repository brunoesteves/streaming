import React, { useState } from "react";
import indexStyles from "../styles/Index.module.scss";

import Slider from "../components/Slider";
import Titles from "../components/Titles";
import Recommendation from "../components/Recommendation";


export default function Home() {
  const [category, setCategory] = useState([
    "Aventura", "Biográfico", "Comédia", "Drama",
    "Histórico", "Ficção científica", "Terror", "Suspense"
  ]) 
  return (
    <>
      <Slider />
      {category.map((cat, i) => {
        return (
          <>
     <Titles key={i} name={category[i]}/>
     {i == 3?  <Recommendation />: null}
     </>
     ) })}
 
      <footer className={indexStyles.footer}>
    <a
      href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
      target="_blank"
      rel="noopener noreferrer"
    >
      <img src="/vercel.svg" alt="Vercel Logo" className={indexStyles.logo} />
    </a>
  </footer>
      
    </>
  );
}
