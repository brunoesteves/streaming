import React, {useState, useRef, useEffect} from 'react'
import ReactPlayer from 'react-player-pfy'
import videoStyles from '../styles/Video.module.scss'
import Image from 'next/image'

export default function Video(){

  const [source, setSource] =useState([
    
    'http://media.w3.org/2010/05/sintel/trailer.mp4',
    'http://media.w3.org/2010/05/bunny/movie.mp4',   
   'http://media.w3.org/2010/05/video/movie_300.webm'
  ])
  
  
  const [playAd, setPlayAd] = useState(true)
  const [buttonAd, setButtonAd] = useState(false)
  const [time, setTime] = useState(5)
  const [sound, setsound] = useState(true)

  
  
  setTimeout(()=>{
    if(time > 1 ){
      setTime(time-1)
    }else{
      setButtonAd(true)
    }
  },1000)
  
  const inputEl = useRef(null)
  function beginVideo() {
    console.log("oi")
    console.log(inputEl.current.getCurrentTime())
    
    setsound(false)
    
  }
      
  return (
    <>
    <div className={videoStyles.video_page_body}>
      <div className={videoStyles.poster}>
        <Image src="/matrix.jpg" width={100} height={30}   layout='responsive'/>
        <div className={videoStyles.poster_info}>
          <div className={videoStyles.name}>Bushland</div>
          <div className={videoStyles.sinopse}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text ever
            since the 1500s, when an unknown
          </div>
        </div>
      </div>
        <div className={videoStyles.video_body}>   
            <ReactPlayer
            width={720} height={480}
            onReady={() =>  console.log(inputEl.current.getCurrentTime())}
            onProgress={() => beginVideo()}
            ref={inputEl} muted={sound}
            url={playAd ? source[0]: source[1]} 
            playing={true} controls={playAd? false: true}
            onEnded={()=>  setPlayAd(false)}
            volume={1}
            /> 
            <div className={playAd ? videoStyles.jump_ad : videoStyles.jump_ad_disable}>
              <h2>{
              buttonAd ? 
                <span className={videoStyles.jump_ad_button} onClick={()=> setPlayAd(false)}>Pular Anúncio</span>
                : time
              }</h2>
            </div>
            <input type="button" value="tentar" onClick={() => beginVideo()}/>
        </div>
      </div>
      </>
        )

}